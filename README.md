# License Plate Prediction

Magic trick using a license plate
[http://ideaalab.com/en/prediccion-matricula](http://ideaalab.com/en/prediccion-matricula)

## Getting Started

You can just print the PDF file and assemble as the instruction video. Otherwise you have the templates to make modifications.

## Modification

If you want to modify the templates you will need Corel Draw X6 or newer.

Install the included font to be able to correctly modify the template.

## Author

* **Martin Andersen** - *Idea and templates* - [IDEAA Lab](http://ideaalab.com/en/prediccion-matricula)
* **Several Authors** - *Presentation ideas*

## License

This project is licensed under the [Creative Commons Atribution-ShareAlike license](http://creativecommons.org/licenses/by-sa/4.0/)