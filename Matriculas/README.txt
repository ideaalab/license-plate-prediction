Some files have been designed so they can be directly printed on A3+ / A3 / Tabloid paper size. Like explained on the videoblog.
Some others didnt have time to make design like that (Simple template), but instead each page is alone.
You can print this one per page, or configure the printer to print more than one per page to save on paper.

Each PDF have 11 pages:
-Page 1: Prediction page (long) -> print 9 copies
-Page 2 to 10: other pages (short) -> print 1 copy each
-Page 11: bottom frame page -> print 1 copy

You will have to assemble, from top to bottom:

1. front cover (transparent)
2. short
3. long
[...]
18. short
19. long
20. bottom frame page
21. back cover